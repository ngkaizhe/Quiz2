﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseLookController : MonoBehaviour
{
    public Transform playerBody;

    float xRotation = 0f;
    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        float mouseSensitivity = 100f;
        float mouseX = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
        float mouseY = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;

        xRotation = Mathf.Clamp(xRotation - mouseY, -30f, 30f);

        float yRotation = transform.rotation.y;
        yRotation -= mouseX;

        transform.localRotation = Quaternion.Euler(xRotation, 0 , 0);
        playerBody.Rotate(new Vector3(0, 1, 0), mouseX);
    }
}
