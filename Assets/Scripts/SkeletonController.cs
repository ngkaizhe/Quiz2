﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkeletonController : MonoBehaviour
{
    Animator m_animator;

    public float m_speed = 0.0f;
    float max_speed = 1.0f;
    float min_speed = 0.0f;
    bool[] keyPressed = { false, false, false, false};

    public AudioClip screamingClip;
    public AudioSource footstep;


    // Start is called before the first frame update
    void Start()
    {
        m_animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        setInput("skill", KeyCode.Space);
        setInput("death", KeyCode.Z);

        if (Input.GetMouseButtonDown(0))
        {
            m_animator.SetBool("attack", true);
        }
        else if (Input.GetMouseButtonUp(0))
        {
            m_animator.SetBool("attack", false);
        }

        speedInput();
    }

    void setInput(string name, KeyCode key)
    {
        if (Input.GetKeyDown(key))
        {
            m_animator.SetBool(name, true);
        }
        else if (Input.GetKeyUp(key))
        {
            m_animator.SetBool(name, false);
        }
    }

    void speedInput()
    {
        float speedMultiple = 0.5f;
        if (Input.GetKey(KeyCode.W))
        {
            m_speed = Mathf.Clamp(m_speed + Time.deltaTime, min_speed, max_speed);
            transform.Translate(new Vector3(0, 0, 1) * m_speed * speedMultiple);
            keyPressed[0] = true;
        }
        else keyPressed[0] = false;

        if (Input.GetKey(KeyCode.S))
        {
            m_speed = Mathf.Clamp(m_speed + Time.deltaTime, min_speed, max_speed);
            transform.Translate(new Vector3(0, 0, -1) * m_speed * speedMultiple);
            keyPressed[1] = true;
        }
        else keyPressed[1] = false;

        if (Input.GetKey(KeyCode.A))
        {
            m_speed = Mathf.Clamp(m_speed + Time.deltaTime, min_speed, max_speed);
            transform.Translate(new Vector3(-1, 0, 0) * m_speed * speedMultiple);
            keyPressed[2] = true;
        }
        else keyPressed[2] = false;

        if (Input.GetKey(KeyCode.D))
        {
            m_speed = Mathf.Clamp(m_speed + Time.deltaTime, min_speed, max_speed);
            transform.Translate(new Vector3(1, 0, 0) * m_speed * speedMultiple);
            keyPressed[3] = true;
        }
        else keyPressed[3] = false;


        if(!keyPressed[0] && !keyPressed[1] && !keyPressed[2] && !keyPressed[3])
            m_speed = Mathf.Clamp(m_speed - Time.deltaTime * 0.5f, min_speed, max_speed);

        m_animator.SetFloat("speed", m_speed / max_speed);

        if (m_speed / max_speed > 0)
        {
            footstep.volume = 0.2f;
        }
        else footstep.volume = 0;

        footstep.pitch = (m_speed / max_speed * 6.0f) - 3.0f;
    }

    void screamAnimationEvent()
    {
        AudioSource.PlayClipAtPoint(screamingClip, transform.position);
    }
    
}
